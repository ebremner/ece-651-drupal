# Setup Drupal for MacOS

- Clone repo, ensure that macos (it is default) is downloaded.
- cd into ece-651-drupal
- ``docker-compose up -d``
- ``cp default.bashrc html``
- ``docker-compose exec php bash``
- ``source default.bashrc``
- ``rcsetup``
- ``exit``
- ``docker-compose exec php bash``
- ``d9rp``
- Then visit site at http://drupal.docker.localhost:8000